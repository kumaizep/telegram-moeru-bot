from __future__ import annotations
import io

from PIL import Image


def compress_image(image_data: bytes) -> bytes:
    image = Image.open(io.BytesIO(image_data))
    image.thumbnail((1280, 1280), Image.LANCZOS)
    out_io = io.BytesIO()
    image.save(out_io, format="WEBP")
    return out_io.getvalue()
