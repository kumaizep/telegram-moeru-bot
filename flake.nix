{
  description = "Telegram Moeru Bot";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils } @ inputs:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        dev-shell-build-inputs = [
          pkgs.pyright
          pkgs.python3Packages.autopep8
          pkgs.rnix-lsp
          pkgs.poetry
          pkgs.chromedriver
          pkgs.chromium
        ];
        poetry-overrides = pkgs.poetry2nix.overrides.withDefaults (self: super: {
          attrs = super.attrs.overridePythonAttrs (
            old: {
              nativeBuildInputs = (old.nativeBuildInputs or [ ]) ++ [
                self.hatchling
                self.hatch-fancy-pypi-readme
                self.hatch-vcs
              ];
            }
          );
        });
      in
      rec {
        packages.default = pkgs.poetry2nix.mkPoetryApplication {
          projectDir = ./.;
          propagatedBuildInputs = [ pkgs.chromedriver pkgs.chromium ];
          overrides = poetry-overrides;
        };

        devShells.default = (pkgs.poetry2nix.mkPoetryEnv {
          projectDir = ./.;
          overrides = poetry-overrides;
        }).env.overrideAttrs (oldAttrs: {
          buildInputs = dev-shell-build-inputs;
        });
      }) // {
        nixosModules.default = import ./nixos/moerud.nix {
          moeru-bot-packages = self.packages;
        };
      };
}
