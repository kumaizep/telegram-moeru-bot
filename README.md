# Telegram Moeru Bot

This is a Telegram bot that fetches and posts images on a channel admin's behave.

## Development

This project is developed with Nix Flakes and Poetry.
To run the bot in a development shell, export necessary environment variables and run `poetry run`:

```bash
nix develop .#
export MOERU_BOT_TOKEN="token"
export MOERU_BOT_CHANNEL="channel id"
export MOERU_BOT_ADMIN="admin username"
poetry run moeru-bot
```

## Deployment

A NixOS module is included under `nixos/`.
The `moerud` service can be activated by adding the following in config.

```nix
services.moerud = {
    enable = true;
    environmentFile = "/secrets/moerud.env";
}
```

## Usage

### Image Fetching

From the admin's private chat with the bot, send any message with URLs and / or text links.
The bot fetches the images and sends back prompts for the admin to approve or deny.

### Admin Commands

- `/admin` sets the chat id of the admin.  This is used for exception reporting.
- `/pixiv <PHPSESSID> <device_token>` sets cookies for Pixiv.

## Supported Sites

- Twitter
- Pixiv

## License

This project is licensed under GNU Affero General Public License v3.0.
See `LICENSE` file for details.
